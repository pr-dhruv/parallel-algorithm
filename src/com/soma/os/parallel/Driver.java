package com.soma.os.parallel;

import com.soma.os.sequential.MergeSortSequential;

import java.util.Arrays;
import java.util.Random;

/**
 * @author Mahendra Prajapati
 * @since 21-09-2020
 */
public class Driver {

    private static Random random = new Random();
    private static int MAX_THRESHOLD = 1_00_000;
    private static int MAX_SIZE = 100000000;

    public static void main(String[] args) {
        int numberOfThreads = Runtime.getRuntime().availableProcessors();
        System.out.println("Numbers of threads : " + numberOfThreads);

        int[] array = new int[]{4, 2, 6, 5, 44, 78, -4, 0, 1};

        int[] numbers = createRandomArray(15);
        MergeSortSequential mergeSort = new MergeSortSequential(numbers);
        System.out.println("Unsorted Array : " + mergeSort);
        mergeSort.sort();
        System.out.println("Sorted Array : " + mergeSort);

        int[] test = createRandomArray(MAX_SIZE);
        long startingTime = 0;
        long endTime = 0;

        mergeSort = new MergeSortSequential(test);
        startingTime = System.currentTimeMillis();
        mergeSort.sort();
        endTime = System.currentTimeMillis() - startingTime;
        System.out.println("Total time in sequential algorithm : " + (endTime / 1000) + " s");

        ParallelMergeSort parallelMergeSort = new ParallelMergeSort(test);
        startingTime = System.currentTimeMillis();
        parallelMergeSort.sort();
        endTime = System.currentTimeMillis() - startingTime;
        System.out.println("Total time in parallel algorithm : " + (endTime / 1000) + " s");

    }

    private static int[] createRandomArray(int size) {
        int[] tempArray = new int[size];
        for (int i = 0; i < size; i++)
            tempArray[i] = random.nextInt(MAX_THRESHOLD + 1);
        return tempArray;
    }
}
