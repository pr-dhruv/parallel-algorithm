package com.soma.os.sequential;

import com.soma.os.parallel.ParallelMergeSort;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {12, 11, 13, 5, 6, 7};
        System.out.println("Unsorted : " + Arrays.toString(array));

        ParallelMergeSort ps = new ParallelMergeSort(array);
        ps.sort();
        System.out.println("Sorted : " + Arrays.toString(array));
    }
}
