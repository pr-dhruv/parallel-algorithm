package com.soma.os.parallel;

import java.util.Arrays;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public class ParallelMergeSort {

    private int[] array;

    public ParallelMergeSort(int[] array) {
        this.array = array;
    }

    public void parallelSort(int numbersOfThreads) {
        parallelMergeSort(0, this.array.length - 1, numbersOfThreads);
    }

    private void parallelMergeSort(int begin, int end, int numbersOfThreads) {
        // If we have at most one thread in the system then we will go for sequential sorting algorithm
        if(numbersOfThreads <= 1) {
            sortAux(begin, end);
            return;
        }

        int med = (begin + end) / 2;
        Thread leftSubArray = mergeSortParallel(begin, med, numbersOfThreads);
        Thread rightSubArray = mergeSortParallel(med + 1, end, numbersOfThreads);

        leftSubArray.start();
        rightSubArray.start();

        try {
            leftSubArray.join();
            rightSubArray.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        merge(begin, med, end);
    }

    private Thread mergeSortParallel(int begin, int end, int numberOfThreads) {
        return new Thread(() -> {
            parallelMergeSort(begin, end, numberOfThreads / 2);
        });
    }

    public void sort() {
        sortAux(0, array.length - 1);
    }

    private void sortAux(int begin, int end) {
        if (begin < end) {
            // Diving the array from medium
            int med = (begin + end) / 2;

            // Recursive calls for divided arrays
            sortAux(begin, med);
            sortAux(med + 1, end);

            // Merging the divided array
            merge(begin, med, end);
        }
    }

    private void merge(int begin, int med, int end) {
        int leftArraySize = med - begin + 1;
        int rightArraySize = end - med;

        int[] leftArray = new int[leftArraySize];
        int[] rightArray = new int[rightArraySize];

        // Copy the arrays
        for (int i = 0; i < leftArraySize; i++)
            leftArray[i] = array[begin + i];
        for (int i = 0; i < rightArraySize; i++)
            rightArray[i] = array[med + i + 1];

        int i = 0;  // Initial index of left array
        int j = 0;  // Initial index of right array
        int k = begin;  // Initial index of merged array

        // Copying the items by comparing the items
        while (i < leftArraySize && j < rightArraySize) {
            if (leftArray[i] <= rightArray[j])
                array[k++] = leftArray[i++];
            else
                array[k++] = rightArray[j++];
        }

        // Copy the remaining items from left array
        while (i < leftArraySize) {
            array[k++] = leftArray[i++];
        }

        // Copy the remaining items from right array
        while (j < rightArraySize) {
            array[k++] = rightArray[j++];
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
