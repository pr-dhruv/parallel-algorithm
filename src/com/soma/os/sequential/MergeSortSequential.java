package com.soma.os.sequential;

import java.util.Arrays;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public class MergeSortSequential {

    private int[] array;

    public MergeSortSequential(int[] array) {
        this.array = array;
    }

    public void sort() {
        sortAux(0, this.array.length - 1);
    }

    private void sortAux(int begin, int end) {
        if (begin < end) {
            // Diving the array from medium
            int med = (begin + end) / 2;

            // Recursive calls for divided arrays
            sortAux(begin, med);
            sortAux(med + 1, end);

            // Merging the divided array
            merge(begin, med, end);
        }
    }

    private void merge(int begin, int med, int end) {
        int leftArraySize = med - begin + 1;
        int rightArraySize = end - med;

        int[] leftArray = new int[leftArraySize];
        int[] rightArray = new int[rightArraySize];

        // Copy the arrays
        for (int i = 0; i < leftArraySize; i++)
            leftArray[i] = array[begin + i];
        for (int i = 0; i < rightArraySize; i++)
            rightArray[i] = array[med + i + 1];

        int i = 0;  // Initial index of left array
        int j = 0;  // Initial index of right array
        int k = begin;  // Initial index of merged array

        // Copying the items by comparing the items
        while (i < leftArraySize && j < rightArraySize) {
            if (leftArray[i] <= rightArray[j])
                array[k++] = leftArray[i++];
            else
                array[k++] = rightArray[j++];
        }

        // Copy the remaining items from left array
        while (i < leftArraySize) {
            array[k++] = leftArray[i++];
        }

        // Copy the remaining items from right array
        while (j < rightArraySize) {
            array[k++] = rightArray[j++];
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(this.array);
    }



}
